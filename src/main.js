require([
  "esri/map",
  "esri/layers/FeatureLayer",
  "esri/InfoTemplate",
  "esri/geometry/webMercatorUtils",
  "dojo/dom",
  //"./opencagedata.js"
],
  function (Map, FeatureLayer, InfoTemplate, webMercatorUtils, dom) {
    var map = new Map("map", {
      basemap: "gray-vector",
      center: [7.652769, 51.935209],
      zoom: 13,
    });

    var template = new InfoTemplate("${CITY_NAME}");

    var worldCities = new FeatureLayer("https://services.arcgis.com/P3ePLMYs2RVChkJx/ArcGIS/rest/services/World_Cities/FeatureServer/0", {
      outFields: ["CITY_NAME"],
      infoTemplate: template
    });

    map.addLayer(worldCities);

    map.on("load",function(){
      map.on("click", getCoordinates);
    });
  
    var x;
    var y;

    function getCoordinates(evt){
      var mp = webMercatorUtils.webMercatorToGeographic(evt.mapPoint);
      x = mp.x;
      y = mp.y;
      dom.byId("x").innerHTML = "latitude: " + x; 
      dom.byId("y").innerHTML = "longitude: " + y; 
    }

    //getGeoCode(x, y);

  });
