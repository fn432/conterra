//
// see our full nodejs tutorial:
// https://opencagedata.com/tutorials/geocode-in-nodejs
//
// npm install opencage-api-client
const opencage = require('opencage-api-client');

module.exports.getGeoCode = function (x, y){
    var koord = x +", " + y;
    console.log(koord);
    opencage.geocode({ q: koord, language: 'de', key: "1f5aa7d5392b4351aeaa6d83eadf550e" }).then(data => {
        // console.log(JSON.stringify(data));
        if (data.status.code == 200 && data.results.length > 0) {
            // 1330 Middle Avenue, Menlo Park, Californie 94025, États-Unis d'Amérique

            console.log(data.results[0].formatted);
        }
    }).catch(error => {
        console.log('error', error.message);
    });
}
